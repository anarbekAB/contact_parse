<?php

namespace App\Object\Vk;

class Post
{
    /** @var  int */
    protected $id;

    /** @var  string */
    protected $postType;

    /** @var  string | null */
    protected $textUser;

    /** @var  string | null */
    protected $textCopy;

    /** @var  int */
    protected $commentCount;

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getPostType() : string
    {
        return $this->postType;
    }

    /**
     * @param string $postType
     */
    public function setPostType(string $postType)
    {
        $this->postType = $postType;
    }

    /**
     * @return null|string
     */
    public function getTextUser()
    {
        return $this->textUser;
    }

    /**
     * @param null|string $textUser
     */
    public function setTextUser($textUser)
    {
        $this->textUser = $textUser;
    }

    /**
     * @return null|string
     */
    public function getTextCopy()
    {
        return $this->textCopy;
    }

    /**
     * @param null|string $textCopy
     */
    public function setTextCopy($textCopy)
    {
        $this->textCopy = $textCopy;
    }

    /**
     * @return int
     */
    public function getCommentCount() : int
    {
        return $this->commentCount;
    }

    /**
     * @param int $commentCount
     */
    public function setCommentCount(int $commentCount)
    {
        $this->commentCount = $commentCount;
    }

    /**
     * @param array $data
     * @return Post
     */
    public static function fromArray(array $data) : self
    {
        $obj = new self();

        $obj->setId($data['id']);
        $obj->setPostType($data['post_type']);
        $obj->setTextUser($data['text']);
        $obj->setTextCopy(isset($data['copy_history']) ? $data['copy_history'][0]['text'] : null);
        $obj->setCommentCount($data['comments']['count']);

        return $obj;
    }
}
