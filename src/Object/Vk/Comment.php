<?php

namespace App\Object\Vk;

class Comment
{
    /** @var  int */
    protected $id;

    /** @var  string */
    protected $text;

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getText() : string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text)
    {
        $this->text = $text;
    }

    /**
     * @param array $data
     * @return Comment
     */
    public static function fromArray(array $data) : self
    {
        $obj = new self();

        $obj->setId($data['id']);
        $obj->setText($data['text']);

        return $obj;
    }
}
