<?php

namespace App\Command;

use App\Entity\Email;
use App\Object\Vk\Comment;
use App\Object\Vk\Post;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class VkCommand extends BaseParse
{
    protected $url;

    protected $token;

    protected $version;

    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct('parse:email:vk', 'parse email in vk.com', $entityManager);

        $this->token = $_ENV['VK_TOKEN'];
        $this->version = $_ENV['VK_API_VERSION'];
        $this->url = 'https://api.vk.com';
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $userId = 1;
        while (true) {
            $start = microtime(true);

            $postItems = $this->getWall((string)$userId);
            if (!$postItems) {
                continue;
            }

            /** @var Post $post */
            foreach ($postItems as $post) {
                //search email in user post text
                $searchText = $post->getTextUser() . ' ' . $post->getTextCopy();
                if (strpos($searchText, '@')) {
                    $this->searchAndSaveEmail($searchText);
                }

                //search email in user post comments
                $startComment = microtime(true);
                $comments = $this->getComments((string)$userId, (string)$post->getId());
                $searchText = '';

                /** @var Comment $comment */
                foreach ($comments as $comment) {
                    $searchText .= $comment->getText() . ' ';
                }

                if (strpos($searchText, '@')) {
                    $this->searchAndSaveEmail($searchText);
                }

                //sleep for bypass the limitations of api vk.com
                $endComment = microtime(true);
                usleep(3000000 - ($startComment - $endComment));

                echo 'Post #' . $post->getId() . ' complete' . PHP_EOL;
            }

            //sleep for bypass the limitations of api vk.com
            $end = microtime(true);
            usleep(3000000 - ($end - $start));

            $userId++;
        }
    }

    protected function getWall(string $userId)
    {
        $client = new Client([
            'base_uri' => $this->url,
            'query' => ['v' => $this->version, 'access_token' => $this->token, 'owner_id' => $userId],
            'headers' => null
        ]);

        $request = $client->get('method/wall.get');

        $wallResult = json_decode($request->getBody()->getContents(), true);

        if (isset($wallResult['error'])) {
            return [];
        }

        $postItems = [];
        if ($request->getStatusCode() == 200) {
            foreach ($wallResult['response']['items'] as $item) {
                $postItems[] = Post::fromArray($item);
            }
        }

        return $postItems;
    }

    /**
     * @param string $userId
     * @param string $postId
     * @return array
     */
    protected function getComments(string $userId, string $postId)
    {
        $client = new Client([
            'base_uri' => $this->url,
            'query' => [
                'v' => $this->version,
                'access_token' => $this->token,
                'owner_id' => $userId,
                'post_id' => $postId,
                'count' => 100, //todo fix count 100 comments
                'preview_length' => 0
            ],
            'headers' => null
        ]);

        $request = $client->get('method/wall.getComments');

        $commentsResult = json_decode($request->getBody()->getContents(), true);

        if (isset($commentsResult['error'])) {
            return [];
        }

        $comments = [];
        if ($request->getStatusCode() == 200) {
            foreach ($commentsResult['response']['items'] as $item) {
                $comments[] = Comment::fromArray($item);
            }
        }

        return $comments;
    }

    protected function createEmailDB(string $email, string $resource = 'vk')
    {
        $mail = $this->emailRepository->findOneBy(['email' => $email]);
        if ($mail) {
            return false;
        }

        $mail = new Email();
        $mail->setEmail($email);
        $mail->setResource($resource);

        $this->em->persist($mail);
        $this->em->flush();

        return true;
    }

    /**
     * @param string $search
     */
    protected function searchAndSaveEmail(string $search) : void
    {
        $emails = [];

        preg_match_all(
            '/([a-z0-9\.]{1,50}@[a-z0-9]{1,50}\.[a-z]{1,5})/ims',
            $search,
            $emails
        );

        foreach ($emails[0] as $email) {
            $this->createEmailDB($email);
        }
    }
}
