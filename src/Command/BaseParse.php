<?php

namespace App\Command;

use App\Entity\Email;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

abstract class BaseParse extends Command
{
    /** @var  string */
    protected $commandName;

    /** @var  string */
    protected $commandDescription;

    /** @var EntityManagerInterface */
    protected $em;

    /** @var \App\Repository\EmailRepository */
    protected $emailRepository;

    public function __construct(
        string $commandName,
        string $commandDescription,
        EntityManagerInterface $entityManager
    ) {
        $this->commandName = $commandName;
        $this->commandDescription = $commandDescription;
        $this->em = $entityManager;
        $this->emailRepository = $this->em->getRepository(Email::class);
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName($this->commandName);
        $this->setDescription($this->commandDescription);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //execute command
    }
}
