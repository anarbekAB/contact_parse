<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Email
 * @package App\Entity
 *
 * @ORM\Entity(repositoryClass="App\Repository\EmailRepository")
 * @ORM\Table(name="emails", indexes={@ORM\Index(name="search_idx", columns={"email", "resource"})})
 */
class Email
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     */
    protected $email;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     */
    protected $resource;

    public function getId() : int
    {
        return $this->id;
    }

    public function getEmail() : string
    {
        return $this->email;
    }

    public function setEmail(string $email) : self
    {
        $this->email = $email;

        return $this;
    }

    public function getResource() : string
    {
        return $this->resource;
    }

    public function setResource(string $resource) : self
    {
        $this->resource = $resource;

        return $this;
    }
}
